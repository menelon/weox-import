import tempfile
import unittest

from weox_import.cache import Cache, FileSystemCacheStorage, MemoryCacheStorage


class CacheTestCase(unittest.TestCase):

    def test_cached_json_serializable_memory_storage(self):

        cache = Cache(path="/", default_ttl=-1, storage_class=MemoryCacheStorage)

        self.counter = 0

        @cache.cached_json_serializable(filename="testing-request-1.json")
        def test_request_1(foo: str):
            self.counter = self.counter + 1

            return {
                "foo": foo,
                "counter": self.counter,
            }

        @cache.cached_json_serializable(filename="testing-request-2.json")
        def test_request_2(foo: str, bar: str):
            self.counter = self.counter + 1

            return {
                "foo": foo,
                "bar": bar,
                "counter": self.counter,
            }

        assert test_request_1("foo") == {"foo": "foo", "counter": 1}
        assert test_request_1("foo") == {"foo": "foo", "counter": 1}

        assert test_request_2("foo", "bar") == {
            "foo": "foo",
            "bar": "bar",
            "counter": 2,
        }
        assert test_request_2("foo", "bar") == {
            "foo": "foo",
            "bar": "bar",
            "counter": 2,
        }

        assert self.counter == 2

    def test_cached_json_serializable_filesystem_storage(self):

        cache_dir = tempfile.gettempdir()

        cache = Cache(path=cache_dir,
                      default_ttl=-1,
                      storage_class=FileSystemCacheStorage)

        cache.delete("testing-request-1.json")
        cache.delete("testing-request-2.json")

        self.counter = 0

        @cache.cached_json_serializable(filename="testing-request-1.json")
        def test_request_1(foo: str):
            self.counter = self.counter + 1

            return {
                "foo": foo,
                "counter": self.counter,
            }

        @cache.cached_json_serializable(filename="testing-request-2.json")
        def test_request_2(foo: str, bar: str):
            self.counter = self.counter + 1

            return {
                "foo": foo,
                "bar": bar,
                "counter": self.counter,
            }

        assert test_request_1("foo") == {"foo": "foo", "counter": 1}
        assert test_request_1("foo") == {"foo": "foo", "counter": 1}

        assert test_request_2("foo", "bar") == {
            "foo": "foo",
            "bar": "bar",
            "counter": 2,
        }
        assert test_request_2("foo", "bar") == {
            "foo": "foo",
            "bar": "bar",
            "counter": 2,
        }

        assert self.counter == 2

        cache.delete("testing-request-1.json")
        cache.delete("testing-request-2.json")

    def test_cached_xml_memory_storage(self):

        cache = Cache(path="/", default_ttl=-1, storage_class=MemoryCacheStorage)

        self.counter = 0

        assert not cache.exists("testing-request-foo.txt")
        assert not cache.exists("testing-request-foo-bar.txt")

        @cache.cached(filename="testing-request-{}.txt")
        def test_request_1(foo: str):
            self.counter = self.counter + 1

            return f"{foo} {self.counter}"

        @cache.cached(filename="testing-request-{}-{}.txt")
        def test_request_2(foo: str, bar: str):
            self.counter = self.counter + 1

            return f'{foo} {bar} {self.counter}'

        assert test_request_1("foo") == "foo 1"
        assert test_request_1("foo") == "foo 1"

        assert test_request_2("foo", "bar") == "foo bar 2"
        assert test_request_2("foo", "bar") == "foo bar 2"

        assert cache.exists("testing-request-foo.txt")
        assert cache.exists("testing-request-foo-bar.txt")

        assert self.counter == 2
