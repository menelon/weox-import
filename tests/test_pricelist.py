from datetime import datetime
from decimal import Decimal

from weox_import import Pricelist, Product, Specification
from weox_import import locales


def test_no_products():

    pl = Pricelist('test', '1.0')

    result, error = pl.validate()

    assert result is False


def test_min_product():

    pl = Pricelist('test', '1.0')

    product = Product('1')
    product.add_category('Root')
    product.add_name('Test Product')

    pl.add_product(product)

    result, error = pl.validate()

    assert result is True


def test_default_category():

    pl = Pricelist('test', '1.0')

    product = Product('1')
    product.add_name('Test Product')

    pl.add_product(product)

    result, error = pl.validate()

    assert result is True

    assert len(pl.products[0].find('categories')) == 1

    assert pl.products[0].find('categories')[0].text == 'Unsorted'


def test_default_brand():

    pl = Pricelist('test', '1.0')

    product = Product('1')
    product.add_name('Test Product')

    pl.add_product(product)

    result, error = pl.validate()

    assert result is True

    assert pl.products[0].find('brand').text == 'Noname'


def test_max_product():

    pl = Pricelist('test', '1.0')

    product = Product('1')
    product.set_brand('Asus')
    product.set_supplier_code('supplier_code')

    product.add_ean('0075678164125')
    product.add_ean('4026908001150')
    product.add_ean('30074576')

    product.add_category('Root')
    product.add_category('Toor')

    product.add_name('Name 1')
    product.add_name('Name 2', locales.LOCALE_RU)
    product.add_name('Название 3', locales.LOCALE_RU)
    product.add_name('Name 4', locales.LOCALE_EN)

    product.add_description('Description 1')
    product.add_description('Description 2', locales.LOCALE_RU)
    product.add_description('Описание 3', locales.LOCALE_RU)
    product.add_description('Description 4', locales.LOCALE_EN)
    product.add_description('<a href="#">link ссылка</a>', locales.LOCALE_RU)

    product.set_quantity(1, 'One')
    product.set_stock_date(datetime.now().date().isoformat())

    product.set_warranty(24, '24 Months')

    product.set_price(Decimal('100.5'), 'EUR', vat=0)
    product.set_retail_price(Decimal('200'), 'USD')
    product.set_old_retail_price(Decimal('250'), 'USD')

    product.add_image('http://example.com/example1.jpg')
    product.add_image('http://example.com/example2.jpg')

    product.set_weox_product_info_url('http://localhost:8888/product_info/1')

    sp1 = Specification()
    sp1.add_name('Color')
    sp1.add_value('Red')
    product.add_specification(sp1)

    sp2 = Specification()
    sp2.add_name('Color', locales.LOCALE_EN)
    sp2.add_name('Цвет', locales.LOCALE_RU)
    sp2.add_name('Tsvet')
    sp2.add_value('Red')
    sp2.add_value('Red', locales.LOCALE_EN)
    sp2.add_value('Красный', locales.LOCALE_RU, 'Штука')
    product.add_specification(sp2)

    pl.add_product(product)

    result, error = pl.validate()

    assert result is True
