from decimal import Decimal

from weox_import.utils import check_ean, is_empty_string, is_numeric


def test_check_invalid_ean():

    assert not check_ean('477201309844B')
    assert not check_ean('477201309844')
    assert not check_ean('XXXX')
    assert not check_ean('')


def test_check_valid_ean():

    assert '190198048387' == check_ean('190198048387')
    assert '190198048387' == check_ean('190-19804-8387')
    assert '190198048387' == check_ean('19019 8048387')
    assert '190198048387' == check_ean('190198048387X')
    assert 190198048387 == check_ean(190198048387)


def test_string_is_empty():

    assert is_empty_string(None)
    assert is_empty_string(False)
    assert is_empty_string('')
    assert is_empty_string(' ')
    assert is_empty_string('    ')


def test_string_is_not_empty():

    assert not is_empty_string(True)
    assert not is_empty_string(1)
    assert not is_empty_string('a')
    assert not is_empty_string(' ab ')
    assert not is_empty_string('  c  ')


def test_is_numeric():

    assert is_numeric(1)
    assert is_numeric(1111)
    assert is_numeric('1')
    assert is_numeric('1111')
    assert is_numeric(Decimal('123'))

    assert not is_numeric(Decimal('123.12'))
    assert not is_numeric(None)
    assert not is_numeric('')
    assert not is_numeric(False)
    assert not is_numeric(True)
    assert not is_numeric('12a123')
    assert not is_numeric('X12')
