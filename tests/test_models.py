import arrow
from weox_import import Product, locales
from weox_import.models import Specification


def test_product_quantity():

    product = Product('1')
    product.set_quantity(1)

    assert product.quantity == 1
    assert product.quantity_str is None


def test_product_quantity_str():
    product = Product('1')

    product.set_quantity(1, 'one')
    assert product.quantity == 1
    assert product.quantity_str == 'one'


def test_product_quantity_equals_to_quantity_str():
    product = Product('1')

    product.set_quantity(1, '1')
    assert product.quantity == 1
    assert product.quantity_str is None


def test_stock_date_in_future():

    product = Product('1')

    utc = arrow.utcnow()
    tomorrow = utc.shift(days=1)

    product.set_stock_date(tomorrow.format('YYYY-MM-DD'))

    assert product.stock_date == tomorrow.date()


def test_stock_date_in_past():

    product = Product('1')

    utc = arrow.utcnow()
    yesterday = utc.shift(days=-1)

    product.set_stock_date(yesterday.format('YYYY-MM-DD'))

    assert not product.stock_date


def test_minimal_product_to_json():
    product = Product('1')

    assert product.to_json() == {
        'id': '1',
        'images': [],
        'eans': [],
        'specifications': [],
        'names': [],
        'descriptions': [],
    }


def test_full_product_to_json():
    product = Product('1')
    product.add_image('http://www.example.com/image1.jpg')
    product.add_ean('190198048387')
    product.add_name('Name', locales.LOCALE_EN)
    product.add_name('Имя', locales.LOCALE_RU)
    product.add_description('Description', locales.LOCALE_EN)
    product.add_description('Описание', locales.LOCALE_RU)

    spec = Specification()
    spec.add_name('Name', locales.LOCALE_EN)
    spec.add_value('Value', locales.LOCALE_EN)
    product.add_specification(spec)

    assert product.to_json() == {
        'id':
            '1',
        'images': ['http://www.example.com/image1.jpg'],
        'eans': ['190198048387'],
        'specifications': [{
            'names': [{
                'locale': locales.LOCALE_EN,
                'name': 'Name',
            }],
            'values': [{
                'locale': locales.LOCALE_EN,
                'value': 'Value',
                'unit': None,
            }],
        }],
        'names': [{
            'name': 'Name',
            'locale': locales.LOCALE_EN,
        }, {
            'locale': locales.LOCALE_RU,
            'name': 'Имя',
        }],
        'descriptions': [{
            'description': 'Description',
            'locale': locales.LOCALE_EN,
        }, {
            'description': 'Описание',
            'locale': locales.LOCALE_RU,
        }],
    }
