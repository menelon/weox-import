"""Packaging settings."""

from codecs import open
from os.path import abspath, dirname, join
from subprocess import call

from setuptools import Command, find_packages, setup

this_dir = abspath(dirname(__file__))
with open(join(this_dir, 'README.rst'), encoding='utf-8') as file:
    long_description = file.read()


class RunTests(Command):
    """Run all tests."""
    description = 'run tests'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        """Run all tests!"""
        errno = call(['pytest', 'tests'])
        raise SystemExit(errno)


class RunFlake8(Command):
    """Run all tests."""
    description = 'run flake8'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        errno = call(['flake8', '--count'])
        raise SystemExit(errno)


setup(
    name='weox_import',
    version='1.1.6',
    description='Weox import.',
    long_description=long_description,
    author='Sergei Kuznetsov',
    author_email='work@setor.net',
    license='UNLICENSED',
    classifiers=[
        'Intended Audience :: Developers',
        'Topic :: Utilities',
        'License :: Public Domain',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    keywords='cli',
    packages=find_packages(exclude=['docs', 'tests*']),
    install_requires=[
        'lxml==4.2.5',
        'gtin-validator==1.0.3',
        'fastapi==0.49.0',
        'uvicorn==0.11.3',
        'starlette==0.12.9',
        'simplejson==3.17.0',
        'arrow==0.15.5',
    ],
    extras_require={
        'test': [
            'coverage',
            'pytest',
            'pytest-cov',
            'flake8',
        ],
    },
    cmdclass={
        'test': RunTests,
        'flake': RunFlake8,
    },
)
