import logging
import os
import time
from abc import ABC, abstractmethod
from typing import Optional

import simplejson as simplejson

logger = logging.getLogger("weox_import_cache")

CACHE_DISABLED_TTL = 0
CACHE_UNLIMITED_TTL = -1


class Cache(object):

    def __init__(self, path, default_ttl: int = CACHE_DISABLED_TTL, storage_class=None):
        self.path = path
        self.default_ttl = default_ttl
        self.unlimited_ttl = False

        if not storage_class:
            storage_class = FileSystemCacheStorage

        self.storage = storage_class()

    def make_cache_path(self, filename: str) -> str:
        return os.path.join(self.path, filename)

    def set_unlimited_ttl(self, value: bool = True):
        self.unlimited_ttl = value

    def get_cache_ttl(self, ttl: Optional[int]) -> int:
        if self.unlimited_ttl:
            return CACHE_UNLIMITED_TTL
        else:
            return self.default_ttl if ttl is None else ttl

    def cached_json_serializable(self, filename: str, ttl: int = None):
        """
        Cache dict as serialized json
        """

        def cached_json_serializable_decorator(func):

            def wrapped(*args, **kwargs):
                cache_ttl = self.get_cache_ttl(ttl)
                filename_formatted = filename.format(*args)
                result = self.load(filename_formatted, cache_ttl)

                if result:
                    result = self._unserialize_json(result)
                else:
                    result = func(*args, **kwargs)
                    if result is not None:
                        json = self._serialize_json(result)
                        self.save(filename_formatted, json, cache_ttl)

                return result

            return wrapped

        return cached_json_serializable_decorator

    def cached(self, filename: str, ttl: int = None):
        """
        Cache any text. Example: raw xml
        """

        def cached_decorator(func):

            def wrapped(*args, **kwargs):
                cache_ttl = self.get_cache_ttl(ttl)
                filename_formatted = filename.format(*args)
                result = self.load(filename_formatted, cache_ttl)

                if not result:
                    result = func(*args, **kwargs)
                    if result is not None:
                        self.save(filename_formatted, result, cache_ttl)

                return result

            return wrapped

        return cached_decorator

    def _serialize_json(self, data):
        logger.info(f'Serializing json')
        return simplejson.dumps(data, encoding="utf-8")

    def _unserialize_json(self, data):
        logger.info(f'Unserializing json')
        return simplejson.loads(data, encoding="utf-8")

    def load(self, filename: str, ttl: int) -> Optional[str]:
        if ttl == CACHE_DISABLED_TTL:
            return

        cache_path = self.make_cache_path(filename)
        logger.info(f'Loading {filename} from cache {cache_path}')
        result = self.storage.load(cache_path, ttl)

        if not result:
            logger.info(f'Not found {filename} in cache {cache_path}')

        return result

    def save(self, filename: str, result: str, ttl: int):
        if ttl == CACHE_DISABLED_TTL:
            return

        cache_path = self.make_cache_path(filename)
        logger.info(f'Saving {filename} cache {cache_path}')
        self.storage.save(key=cache_path, result=result, ttl=ttl)

    def exists(self, filename: str) -> bool:
        return self.storage.exists(self.make_cache_path(filename))

    def delete(self, filename: str):
        if self.exists(filename):  # Not safe
            self.storage.delete(self.make_cache_path(filename))


class CacheStorage(ABC):

    @abstractmethod
    def save(self, key: str, result, ttl: int):
        pass

    @abstractmethod
    def load(self, key: str, ttl: int) -> Optional[str]:
        pass

    @abstractmethod
    def exists(self, key: str) -> bool:
        pass

    @abstractmethod
    def delete(self, key: str):
        pass


class FileSystemCacheStorage(CacheStorage):

    def save(self, key: str, result: str, ttl: int):
        if self.exists(key):
            self.delete(key)  # Not safe

        with open(key, 'w', encoding='utf-8') as out:
            out.write(result)

    def load(self, key: str, ttl: int) -> Optional[str]:
        if not self.exists(key):
            return None

        modification_time = int(os.path.getmtime(key))
        current_time = int(time.time())
        secs_elasped = current_time - modification_time

        if secs_elasped > ttl != CACHE_UNLIMITED_TTL:
            self.delete(key)
            return None

        with open(key, 'r', encoding='utf-8') as file:
            return file.read()

    def exists(self, key: str) -> bool:
        return os.path.isfile(key)

    def delete(self, key: str):
        os.remove(key)


class MemoryCacheStorage(CacheStorage):

    def __init__(self):
        super().__init__()

        self.cache = dict()

    def save(self, key: str, result: str, ttl: int):
        self.cache[key] = result

    def load(self, key: str, ttl: int) -> Optional[str]:
        return self.cache.get(key)

    def exists(self, key: str) -> bool:
        return key in self.cache

    def delete(self, key: str):
        del self.cache[key]
