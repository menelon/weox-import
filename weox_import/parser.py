import logging
from abc import ABC, abstractmethod
from typing import Generator

from weox_import import Pricelist, Product
from weox_import.exceptions import NotImplementedException


class WeoxParser(ABC):

    def __init__(self, supplier_name: str, parser_version: str):

        self.supplier_name = supplier_name
        self.parser_version = parser_version
        self.logger = logging.getLogger(supplier_name)

    def parse(self):
        self.logger.info(f'{self.supplier_name} parser started in production mode')

        pricelist = Pricelist(self.supplier_name, self.parser_version)
        pricelist.add_products(self.parse_pricelist())
        pricelist.publish()

    def parse_development(self, publish_dir: str):
        self.logger.info(f'{self.supplier_name} parser started in development mode')

        pricelist = Pricelist(self.supplier_name, self.parser_version)
        pricelist.add_products(self.parse_pricelist())

        pricelist.publish(
            publish_dir=publish_dir,
            publish_file_name='weox_pricelist.xml',
            pretty_print=True,
        )

    @abstractmethod
    def parse_pricelist(self) -> Generator[Product, None, None]:
        raise NotImplementedException()

    def download_picture(self, picture_id: str):
        raise NotImplementedException()

    def download_product_info(self, product_id: str) -> Product:
        raise NotImplementedException()
