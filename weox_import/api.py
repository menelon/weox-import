from fastapi import FastAPI
from starlette.responses import StreamingResponse
from weox_import.parser import WeoxParser
from weox_import.version import __version__ as weox_import_version


def make_api(parser: WeoxParser):
    app = FastAPI()

    @app.get("/")
    async def root():
        return {
            "supplier_name": parser.supplier_name,
            "parser_version": parser.parser_version,
            "weox_import_version": weox_import_version,
        }

    @app.get("/picture/{id}", response_class=StreamingResponse)
    async def download_picture(picture_id: str):
        return parser.download_picture(picture_id)

    @app.get("/product_info/{product_id}")
    async def download_product_info(product_id: str):
        product = parser.download_product_info(product_id)

        return product.to_json()

    return app
