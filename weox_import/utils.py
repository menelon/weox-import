import re
from typing import Union

from lxml import etree
from lxml.etree import CDATA

from gtin.validator import is_valid_GTIN


def add_text_node(el, name: str, value, attrs: dict = None, required: bool = True):

    if not required and value is None:
        return

    el = etree.SubElement(el, name)

    if value is None:
        pass
    elif isinstance(value, str):
        el.text = value
    elif isinstance(value, CDATA):
        el.text = value
    else:
        el.text = str(value)

    if attrs:
        for name, value in attrs.items():
            if value is not None:
                el.set(name, str(value))

    return el


def check_ean(ean: Union[str, int]) -> Union[str, None]:

    if not is_numeric(ean):
        ean = re.sub(r'[^\d]+', '', ean)

    if is_valid_GTIN(ean):
        return ean
    else:
        return None


def is_empty_string(value) -> bool:

    if value is None or value is False:
        return True

    if not isinstance(value, str):
        value = str(value)

    value = value.strip(' ')

    return value == ''


def is_numeric(value) -> bool:

    if value is None or isinstance(value, bool):
        return False

    return str(value).isdigit()
