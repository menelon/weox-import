import logging
import os
from datetime import datetime
from typing import Iterable

from lxml import etree

from weox_import import settings
from weox_import.exceptions import InvalidFormatError
from weox_import.models import Product
from weox_import.utils import add_text_node
from weox_import.validator import Validator
from weox_import.version import __version__ as weox_import_version

log = logging.getLogger('weox_pricelist')


class Pricelist(object):

    def __init__(self, supplier_name: str, parser_version: str):

        log.info(f'Starting {supplier_name} v{parser_version} import.')

        self.timesatmp = datetime.utcnow()
        self.supplier_name = supplier_name

        self.root = etree.Element('weox_import')
        self.meta = etree.SubElement(self.root, 'meta')
        self.products = etree.SubElement(self.root, 'products')

        self.add_meta('version', weox_import_version)
        self.add_meta('supplier', supplier_name)
        self.add_meta('parser_version', parser_version)
        self.add_meta('time', self.timesatmp.isoformat())

        self.default_currency = self.add_meta('currency', settings.DEFAULT_CURRENCY)
        self.default_vat = self.add_meta('vat', settings.DEFAULT_VAT)
        self.default_locale = self.add_meta('locale', settings.DEFAULT_LOCALE)

        self.generation_time = self.add_meta('gen_time', '')

    def set_default_currency(self, currency: str):
        self.default_currency.text = currency

    def set_default_vat(self, vat: int):
        self.default_vat.text = str(vat)

    def add_meta(self, name: str, value: str):
        return add_text_node(self.meta, name, value)

    def add_product(self, product: Product):
        el = etree.SubElement(self.products, 'product')

        add_text_node(el, 'id', product.id)
        add_text_node(el,
                      'brand',
                      product.brand if product.brand else settings.DEFAULT_BRAND,
                      required=False)
        add_text_node(el, 'sku', product.sku, required=False)
        add_text_node(el, 'supplier_code', product.supplier_code, required=False)

        inventory = etree.SubElement(el, 'inventory')
        if product.price:
            add_text_node(inventory, 'price', product.price, {
                'c': product.price_currency,
                'v': product.price_vat,
            })

        if product.retail_price:
            add_text_node(inventory, 'retail_price', product.retail_price, {
                'c': product.retail_price_currency,
            })

        if product.old_retail_price:
            add_text_node(inventory, 'old_retail_price', product.old_retail_price, {
                'c': product.old_retail_price_currency,
            })

        add_text_node(inventory, 'quantity', product.quantity)
        add_text_node(inventory, 'quantity_str', product.quantity_str, required=False)
        add_text_node(inventory, 'stock_date', product.stock_date, required=False)
        add_text_node(inventory, 'warranty', product.warranty_months, required=False)
        add_text_node(inventory, 'warranty_str', product.warranty_str, required=False)

        if product.eans:
            eans = etree.SubElement(el, 'eans')
            for ean in product.eans:
                add_text_node(eans, 'e', ean)

        categories = etree.SubElement(el, 'categories')
        product_categories = product.categories or [settings.DEFAULT_CATEGORY]
        for category in product_categories:
            add_text_node(categories, 'c', category, required=False)

        names = etree.SubElement(el, 'names')
        for name, locale in product.names:
            add_text_node(names, 'n', name, {
                'l': locale,
            })

        if product.descriptions:
            descriptions = etree.SubElement(el, 'descriptions')
            for description, locale in product.descriptions:
                add_text_node(descriptions, 'd', etree.CDATA(description), {
                    'l': locale,
                })

        if product.images:
            images = etree.SubElement(el, 'images')
            for url in product.images:
                add_text_node(images, 'i', url)

        if product.specifications:
            specifications = etree.SubElement(el, 'specification')

            for specification in product.specifications:
                s = etree.SubElement(specifications, 's')

                for name, locale in specification.names:
                    add_text_node(s, 'n', name, {
                        'l': locale,
                    })

                for value, locale, unit in specification.values:
                    add_text_node(s, 'v', value, {
                        'l': locale,
                        'u': unit,
                    })

        add_text_node(el,
                      'weox_product_info_url',
                      product.weox_product_info_url(),
                      required=False)

    def add_products(self, products: Iterable[Product]):
        for product in products:
            self.add_product(product)

    def to_xml(self, pretty_print: bool = False) -> bytes:
        self._pre_save()

        return etree.tostring(self.root, encoding='utf-8', pretty_print=pretty_print)

    def validate(self, xsd_path: str = None):
        self._pre_save()

        validator = Validator(xsd_path)
        is_valid = validator.validate(self.root)

        return (
            is_valid,
            None if is_valid else validator.last_error(),
        )

    def to_pretty_xml(self) -> str:
        return self.to_xml(pretty_print=True).decode('utf-8')

    def save(self, path: str, pretty_print: bool = False):
        with open(path, 'wb') as out:
            out.write(self.to_xml(pretty_print))

    def publish(self,
                publish_dir: str = None,
                publish_file_name: str = None,
                pretty_print: bool = False) -> str:

        valid, error = self.validate()

        if not publish_dir:
            publish_dir = os.environ.get('PUBLISH_DIR', settings.DEFAULT_PUBLISH_DIR)

        if not publish_file_name:
            timestamp = int(datetime.utcnow().timestamp())
            publish_file_name = f'{timestamp}_{self.supplier_name}.xml'

        xml_path = os.path.join(publish_dir, publish_file_name)

        if valid:
            log.info(f'Saving xml to {xml_path}')
            self.save(xml_path, pretty_print)

            return xml_path
        else:
            log.error(f'XSD Validation error: {error}')

            raise InvalidFormatError(error)

    def _pre_save(self):
        self.generation_time.text = str(self._get_generation_time())

    def _get_generation_time(self) -> int:
        return (datetime.utcnow() - self.timesatmp).seconds
