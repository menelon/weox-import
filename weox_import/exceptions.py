
class WeoxImportError(Exception):
    pass


class InvalidFormatError(WeoxImportError):
    pass


class InvalidEanError(WeoxImportError):
    pass


class EmptyValueError(WeoxImportError):
    pass


class NotImplementedException(WeoxImportError):
    pass
