import datetime
from decimal import Decimal
from typing import List, Optional

import arrow
from weox_import.exceptions import EmptyValueError, InvalidEanError
from weox_import.utils import check_ean, is_empty_string, is_numeric


class Specification(object):

    def __init__(self):
        self.names = []
        self.values = []

    def add_name(self, name: str, locale: str = None):
        if not is_empty_string(name):
            self.names.append((
                name,
                locale,
            ))

    def add_value(self, value: str, locale: str = None, unit: str = None):
        if not is_empty_string(value):
            self.values.append((
                value,
                locale,
                unit,
            ))

    def to_json(self):
        names = [{'name': name, 'locale': locale} for name, locale in self.names]
        values = [{
            'value': value,
            'locale': locale,
            'unit': unit
        } for value, locale, unit in self.values]

        return {
            'names': names,
            'values': values,
        }


class ProductData(object):

    def __init__(self):

        self.id: Optional[str] = None
        self.sku: Optional[str] = None
        self.supplier_code: Optional[str] = None
        self.eans = []

        self.brand: Optional[str] = None

        self.price: Optional[Decimal] = None
        self.price_currency: Optional[str] = None
        self.price_vat: int = 0

        self.retail_price: Optional[Decimal] = None
        self.retail_price_currency: Optional[str] = None

        self.old_retail_price: Optional[Decimal] = None
        self.old_retail_price_currency: Optional[str] = None

        self.quantity: int = 0
        self.quantity_str: Optional[str] = None
        self.stock_date: Optional[datetime.date] = None

        self.warranty_months: Optional[int] = None
        self.warranty_str: Optional[str] = None

        self.categories = []
        self.tags = []

        self.names = []
        self.descriptions = []
        self.specifications: List[Specification] = []

        self.images = []

        self.weox_product_info_url: Optional[str] = None


class Product(object):

    def __init__(self, unique_id: str):
        self._data = ProductData()

        self.set_id(unique_id)

    @property
    def id(self) -> str:
        return self._data.id

    @property
    def sku(self) -> str:
        return self._data.sku

    @property
    def supplier_code(self) -> str:
        return self._data.supplier_code

    @property
    def eans(self):
        return self._data.eans

    @property
    def brand(self) -> str:
        return self._data.brand

    @property
    def price(self) -> Decimal:
        return self._data.price

    @property
    def price_currency(self) -> str:
        return self._data.price_currency

    @property
    def price_vat(self) -> int:
        return self._data.price_vat

    @property
    def retail_price(self) -> Decimal:
        return self._data.retail_price

    @property
    def retail_price_currency(self) -> str:
        return self._data.retail_price_currency

    @property
    def old_retail_price(self) -> Decimal:
        return self._data.old_retail_price

    @property
    def old_retail_price_currency(self) -> str:
        return self._data.old_retail_price_currency

    @property
    def quantity(self) -> int:
        return self._data.quantity

    @property
    def quantity_str(self) -> Optional[str]:
        return self._data.quantity_str

    @property
    def stock_date(self) -> datetime.date:
        return self._data.stock_date

    @property
    def warranty_months(self) -> int:
        return self._data.warranty_months

    @property
    def warranty_str(self) -> str:
        return self._data.warranty_str

    @property
    def categories(self):
        return self._data.categories

    @property
    def tags(self):
        return self._data.tags

    @property
    def names(self):
        return self._data.names

    @property
    def descriptions(self):
        return self._data.descriptions

    @property
    def specifications(self):
        return self._data.specifications

    @property
    def images(self):
        return self._data.images

    def weox_product_info_url(self):
        return self._data.weox_product_info_url

    def set_id(self, unique_id: str):
        if is_empty_string(unique_id):
            raise EmptyValueError('Product ID cannot be empty')

        self._data.id = unique_id

    def set_sku(self, sku: str):
        if not is_empty_string(sku):
            self._data.sku = sku

    def set_supplier_code(self, supplier_code: str):
        if not is_empty_string(supplier_code):
            self._data.supplier_code = supplier_code

    def set_brand(self, brand: str):
        if not is_empty_string(brand):
            self._data.brand = brand

    def add_name(self, name: str, locale: str = None):
        if not is_empty_string(name):
            self._data.names.append((
                name,
                locale,
            ))

    def add_description(self, description: str, locale: str = None):
        if not is_empty_string(description):
            self._data.descriptions.append((
                description,
                locale,
            ))

    def add_image(self, url: str):
        if not is_empty_string(url):
            self._data.images.append(url)

    def add_ean(self, ean: str, raise_exception=False) -> bool:
        valid_ean = check_ean(ean)

        if valid_ean is not None:
            self._data.eans.append(valid_ean)
            return True
        elif raise_exception:
            raise InvalidEanError(f'Invalid EAN {valid_ean}')
        else:
            return False

    def add_category(self, category: str):
        if not is_empty_string(category):
            self._data.categories.append(category)

    def add_tag(self, tag: str):
        if not is_empty_string(tag):
            self._data.tags.append(tag)

    def set_quantity(self, quantity: int, quantity_str: str = None):
        if is_numeric(quantity):
            self._data.quantity = int(quantity)

        if quantity_str is not None:
            if is_empty_string(quantity_str) or str(quantity) == str(quantity_str):
                quantity_str = None

        self._data.quantity_str = quantity_str

    def set_stock_date(self, stock_date: str):
        if not is_empty_string(stock_date):
            dt = arrow.get(stock_date)
            if arrow.utcnow() < dt:
                self._data.stock_date = dt.datetime.date()

    def set_warranty(self, warranty_months: int, warranty_str: str = None):
        if is_numeric(warranty_months):
            self._data.warranty_months = int(warranty_months)

        self._data.warranty_str = warranty_str

    def set_price(self, price: Decimal, currency: str = None, vat: int = None):
        self._data.price = price
        self._data.price_currency = currency
        self._data.price_vat = vat

    def set_retail_price(self, price: Decimal, currency: str = None):
        self._data.retail_price = price
        self._data.retail_price_currency = currency

    def set_old_retail_price(self, price: Decimal, currency: str = None):
        self._data.old_retail_price = price
        self._data.old_retail_price_currency = currency

    def add_specification(self, specification: Specification):
        self._data.specifications.append(specification)

    def set_weox_product_info_url(self, url: str):
        self._data.weox_product_info_url = url

    def autofill_weox_product_info_url(self, api_service_port: int):
        url = f'http://localhost:{api_service_port}/product_info/{self.id}'

        self.set_weox_product_info_url(url)

    def to_json(self):
        specifications = [spec.to_json() for spec in self.specifications]
        names = [{'name': name, 'locale': locale} for name, locale in self.names]
        descriptions = [{
            'description': description,
            'locale': locale,
        } for description, locale in self.descriptions]

        return {
            'id': self.id,
            'images': self.images,
            'eans': self.eans,
            'specifications': specifications,
            'names': names,
            'descriptions': descriptions,
        }
